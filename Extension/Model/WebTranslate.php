<?php
/**
 * Copyright (C) ATHOS TRADER SL <info@athosonline.com>
 */
namespace FacturaScripts\Plugins\WebRedirect\Extension\Model;

use FacturaScripts\Dinamic\Model\Redirect;

/**
 * Description of WebTranslate
 *
 * @author Athos Online <info@athosonline.com>
 */
class WebTranslate
{
    public function delete() {
        return function() {
            if ($this->keytrans === 'permalink' && $this->modelname === 'WebPage' && !is_null($this->modelid)) {
                $redirect = new Redirect();
                $redirect->oldurl = $this->toolBox()->appSettings()->get('webcreator', 'siteurl') . $this->valuetrans;
                $redirect->newurl = $this->toolBox()->appSettings()->get('webcreator', 'siteurl');
                $redirect->saveInsert();
            }
        };
    }

    public function saveUpdate() {
        return function() {
            if ($this->keytrans === 'permalink' && $this->modelname === 'WebPage' && !is_null($this->modelid) && $this->previousData['valuetrans'] !== $this->valuetrans) {
                $redirect = new Redirect();
                $redirect->oldurl = $this->previousData['valuetrans'];
                $redirect->newurl = $this->valuetrans;
                $redirect->saveInsert();
            }
        };
    }
}