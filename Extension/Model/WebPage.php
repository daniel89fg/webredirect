<?php
/**
 * Copyright (C) ATHOS TRADER SL <info@athosonline.com>
 */
namespace FacturaScripts\Plugins\WebRedirect\Extension\Model;

use FacturaScripts\Dinamic\Model\Redirect;

/**
 * Description of WebPage
 *
 * @author Athos Online <info@athosonline.com>
 */
class WebPage
{
    public function delete() {
        return function() {
            $redirect = new Redirect();
            $redirect->oldurl = $this->permalink;
            $redirect->newurl = $this->toolBox()->appSettings()->get('webcreator', 'siteurl');
            $redirect->saveInsert();
        };
    }

    public function saveUpdate() {
        return function() {
            $permalink = \substr($this->permalink, 1);
            print $this->previousData['permalink'] . ' - ' . $permalink;
            if ($this->previousData['permalink'] != $permalink || $this->previousData['pageparent'] !== $this->pageparent) {
                print 'dentro';
                $redirect = new Redirect();
                $redirect->oldurl = '/' . $this->previousData['permalink'];
                $redirect->newurl = $this->permalink;
                //$redirect->saveInsert();
            }
        };
    }
}