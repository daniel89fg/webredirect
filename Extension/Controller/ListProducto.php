<?php
/**
 * Copyright (C) ATHOS TRADER SL <info@athosonline.com>
 */
namespace FacturaScripts\Plugins\WebRedirect\Extension\Controller;

/**
 * Description of ListProducto
 *
 * @author Athos Online <info@athosonline.com>
 */
class ListProducto
{
   public function createViews() {
      return function() {
         $this->addView('ListLogMessage', 'LogMessage', 'log');
      };
   }
}