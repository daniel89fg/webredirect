<?php
/**
 * Copyright (C) ATHOS TRADER SL <info@athosonline.com>
 */
namespace FacturaScripts\Plugins\WebRedirect\Extension\Lib\Portal;

use FacturaScripts\Dinamic\Model\Redirect;
use FacturaScripts\Core\App\AppRouter;

/**
 * Description of UpdateRoutes
 *
 * @author Athos Online <info@athosonline.com>
 */
class UpdateRoutes
{
    public function setRoutesAfter() {
        return function() {
            $appRouter = new AppRouter();
            $redirects = new Redirect();
            foreach ($redirects->all([], [], 0, 0) as $redirect) {
                $appRouter->setRoute($redirect->oldurl, 'PortalHome', '', false);
            }
        };
    }
}