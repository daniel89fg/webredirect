<?php
/**
 * Copyright (C) ATHOS TRADER SL <info@athosonline.com>
 */
namespace FacturaScripts\Plugins\WebRedirect\Extension\Lib\Portal;

use FacturaScripts\Dinamic\Model\Redirect;
use FacturaScripts\Core\Base\DataBase\DataBaseWhere;

/**
 * Description of WebPageData
 *
 * @author Athos Online <info@athosonline.com>
 */
class WebPageData
{
    public function constructBefore() {
        return function($uri) {
            $redirect = new Redirect();
            $where = [new DataBaseWhere('oldurl', $uri)];
            $redirect->loadFromCode('', $where);
            
            if (!empty($redirect->idredirect)) {
                header("Location: " . $redirect->newurl);
                exit();
            }
        };
    }
}