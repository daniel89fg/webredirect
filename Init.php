<?php
/**
 * Copyright (C) ATHOS TRADER SL <info@athosonline.com>
 */
namespace FacturaScripts\Plugins\WebRedirect;

use FacturaScripts\Core\Base\InitClass;
use FacturaScripts\Dinamic\Lib\Portal\UpdateRoutes;

/**
 * Description of Init
 *
 * @author Athos Online <info@athosonline.com>
 */
class Init extends InitClass
{

    public function init()
    {
        $this->loadExtension(new Extension\Model\WebPage());
        $this->loadExtension(new Extension\Controller\ListProducto());
        $this->loadExtension(new Extension\Lib\Portal\WebPageData());
        $this->loadExtension(new Extension\Lib\Portal\UpdateRoutes());

        $fileName = getcwd() . DIRECTORY_SEPARATOR . 'Dinamic'
							 . DIRECTORY_SEPARATOR . 'Model'
							 . DIRECTORY_SEPARATOR . 'WebTranslate.php';

		if (file_exists ($fileName)) {
			$this->loadExtension(new Extension\Model\WebTranslate());
		}
    }

    public function update()
    {
        $routes = new UpdateRoutes();
        $routes->setRoutes();
    }
}