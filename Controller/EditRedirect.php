<?php
/**
 * Copyright (C) ATHOS TRADER SL <info@athosonline.com>
 */
namespace FacturaScripts\Plugins\WebRedirect\Controller;

use FacturaScripts\Core\Lib\ExtendedController\EditController;

/**
 * Description of EditRedirect
 *
 * @author Athos Online <info@athosonline.com>
 */
class EditRedirect extends EditController
{

    /**
     * Returns the model name
     *
     * @return string
     */
    public function getModelClassName()
    {
        return 'Redirect';
    }

    /**
     * Returns basic page attributes
     *
     * @return array
     */
    public function getPageData()
    {
        $pagedata = parent::getPageData();
        $pagedata['menu'] = 'web';
        $pagedata['title'] = 'redirect';
        $pagedata['icon'] = 'fas fa-directions';
        return $pagedata;
    }
}