<?php
/**
 * Copyright (C) ATHOS TRADER SL <info@athosonline.com>
 */
namespace FacturaScripts\Plugins\WebRedirect\Controller;

use FacturaScripts\Core\Lib\ExtendedController;

/**
 * Description of ListCookie
 *
 * @author Athos Online <info@athosonline.com>
 */
class ListRedirect extends ExtendedController\ListController
{
    /**
     * Returns basic page attributes
     *
     * @return array
     */
    public function getPageData()
    {
        $data = parent::getPageData();
        $data['menu'] = 'web';
        $data['title'] = 'redirects';
        $data['icon'] = 'fas fa-directions';
        return $data;
    }
    
    protected function createViews() {
        $this->createViewsRedirects();
    }
    
    /**
     * 
     * @param string $viewName
     */
    protected function createViewsRedirects(string $viewName = 'ListRedirect')
    {
        $this->addView($viewName, 'Redirect', 'redirect', 'fas fa-directions');
    }
}