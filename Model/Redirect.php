<?php
/**
 * Copyright (C) ATHOS TRADER SL <info@athosonline.com>
 */
namespace FacturaScripts\Plugins\WebRedirect\Model;

use FacturaScripts\Core\Model\Base;
use FacturaScripts\Dinamic\Lib\Portal\UpdateRoutes;

/**
 * Description of Redirect
 *
 * @author Athos Online <info@athosonline.com>
 */
class Redirect extends Base\ModelClass
{
    use Base\ModelTrait;

    /**
     *
     * @var serial
     */
    public $idredirect;
    
    /**
     *
     * @var string
     */
    public $oldurl;
    
    /**
     *
     * @var string
     */
    public $newurl;
    
    public static function primaryColumn()
    {
        return 'idredirect';
    }

    public static function tableName()
    {
        return 'webredirect';
    }

    /**
     * Returns the description of the column that is the model's primary key.
     *
     * @return string
     */
    public function primaryDescriptionColumn()
    {
        return 'oldurl';
    }

    public function save()
    {
        $result = parent::save();

        if ($result) {
            $routes = new UpdateRoutes();
            $routes->setRoutes();
        }

        return $result;
    }

    public function delete()
    {
        $result = parent::delete();

        if ($result) {
            $routes = new UpdateRoutes();
            $routes->setRoutes();
        }

        return $result;
    }
}